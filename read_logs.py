import os
import logging
import pandas as pd
from urllib import parse
from datetime import datetime
from collections import defaultdict
from ua_parser import user_agent_parser


ERROR_INITIALIZING_DATAFRAME = 'Failed to initialize dataframe.'


class LogsStatistics:

    def __init__(self, file_path,
                 start_date=None, end_date=None):

        self.file_path = file_path
        self.start_date = start_date
        self.end_date = end_date
        self.dataframe = self.read_file()

    def read_file(self):

        if not os.path.exists(self.file_path):
            logging.error('File not found. Check the file path.')
            return None

        try:
            logging.info('Please wait. File is being read...')
            sep = r'\s(?=(?:[^"]*"[^"]*")*[^"]*$)(?![^\[]*\])'
            data = pd.read_csv(self.file_path, sep=sep, engine='python',
                               usecols=[0, 3, 4, 5, 6, 7, 8, 9, 10],
                               names=['ip', 'time', 'request', 'status',
                                      'bytes', 'referer', 'user_agent',
                                      'request_time',
                                      'upstream_response_time'],
                               na_values='-', header=None)
            data = self.__extended_data_frame(data)
            logging.info('File reading completed successfully.')
            return data
        except:
            logging.error('Error reading file')
            return None

    def __extended_data_frame(self, data):

        dates = data.time
        cleaned_dates = []
        methods, path, host, query_keys, query_values = [], [], [], [], []

        for date in dates:
            try:
                cleaned_date = datetime.strptime(
                    date, '[%d/%b/%Y:%H:%M:%S %z]')
            except:
                cleaned_date = None
            cleaned_dates.append(cleaned_date)

        for index, row in data.iterrows():

            try:
                info = dict(row)
                request_info = info.get('request')

                host_name, path_name, query_key = None, None, []
                query_value, method = [], None

                if request_info:
                    request_info = request_info.split(' ')
                    try:
                        method = request_info[0].replace('"', '')
                        url_details = self.__get_query_list(request_info[1])
                    except:
                        method = None
                        url_details = {}

                    path_name = url_details.get('path')
                    host_name = url_details.get('host')
                    query_key = list(url_details.get('query').keys())
                    query_value = list(url_details.get('query').values())

                path.append(path_name)
                host.append(host_name)
                methods.append(method)
                query_keys.append(query_key)
                query_values.append([val[0] for val in query_value if val])

            except Exception:
                pass

        data['date_time'] = cleaned_dates
        data['path'] = path
        data['host'] = host
        data['method'] = methods
        data['query_keys'] = query_keys
        data['query_values'] = query_values

        return data

    def __get_query_list(self, url):
        details = {}
        try:
            parts = parse.urlparse(url)
            query = parse.parse_qs(parts.query)

            details['host'] = parts.netloc
            details['path'] = parts.path
            details['query'] = query
        except Exception:
            pass
        return details

    def get_stats(self):
        # Compute important statistics here
        df = self.dataframe

        os_stats = defaultdict(int)
        method_stats = defaultdict(int)
        upstream_time_stats = defaultdict(int)
        path_stats = defaultdict(int)
        status_stats = defaultdict(int)
        bytes_stats = defaultdict(int)

        os_list = dict(pd.value_counts(df.user_agent))
        for ua, count in os_list:
            os = user_agent_parser.ParseOS(ua, {}).get('family', 0)
            if os:
                os_stats[os] = count

        method_stats = dict(df.value_counts(df.method))
        upstream_time_stats = dict(df.value_counts(df.upstream_response_time))
        path_stats = dict(df.value_counts(df.upstream_response_time))
        status_stats = dict(df.value_counts(df.status))
        bytes_stats = dict(df.value_counts(df.status))

        return self.__get__cleaned_data(
            os_stats, method_stats, upstream_time_stats,
            path_stats, status_stats, bytes_stats
        )

    def __get__cleaned_data(self, *args, **kwargs):
        pass
